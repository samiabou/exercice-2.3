package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private OutputStream sourceOutputStream;

    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {
        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }

    }

    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    @Override
    public void setSourceOutputStream( OutputStream os) {
           this.sourceOutputStream = os;

    }

    /**
     * {@inheritDoc}
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
    }

    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
