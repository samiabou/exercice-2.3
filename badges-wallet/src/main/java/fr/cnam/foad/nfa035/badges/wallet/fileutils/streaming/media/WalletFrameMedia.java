package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.RandomAccessFile;

/**
 * interface impliquant que pour un media en accès direct,
 * permet de mémoriser le nombre de lignes et de l'incrémenter,
 * afin de l'ajouter aux métadonnées de chaque enregistrement.
 */

public interface WalletFrameMedia extends ResumableImageFrameMedia<RandomAccessFile> {
    /**
     * retourner le nombre de ligne du media
     * @return
     */
    long getNumberOfLines();

    /**
     * Incrémente le nombre de lignes
     */
    void incrementLines();
}