package fr.cnam.foad.nfa035.badges.wallet.model;

import java.util.Objects;

/**
 * cette classe représente les métadonnées à intégrer à un badge
 */
public class DigitalBadgeMetadata {
    private int badgeId;
    private long imageSize;
    private long walletPosition;

    /**
     * Constructeur
     * @param badgeId
     * @param imageSize
     * @param walletPosition
     */
    public DigitalBadgeMetadata(int badgeId, long imageSize, long walletPosition) {
        this.badgeId = badgeId;
        this.imageSize = imageSize;
        this.walletPosition = walletPosition;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return
     */
  @Override

  public boolean equals(Object o){
        if(this == o)return true;
        if (o == null|| getClass() != o.getClass())
            return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return  badgeId ==that.badgeId && walletPosition == that.walletPosition && imageSize == that.imageSize;

  }

  /**
     * setter badgeId
     *
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    /**
     * getter badgeId
     */
    public int getBadgeId() {
        return this.badgeId;
    }
    /**
     * setter de taille d'image
     *
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * getter de la taille d'image
     * @return
     */
    public long getImageSize() {
        return this.imageSize;
    }
    /**
     * setter de la position de badge dans le wallet
     *
     * @param walletposition
     */
    public void setWalletPosition(long walletposition) {
        this.walletPosition = walletPosition;
    }

    /**
     * getter de la position de badge dans le wallet
     * @return
     */
    public long getWalletPosition() {
        return this.walletPosition;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public int hashCode(){
        return Objects.hash(badgeId,walletPosition,imageSize);
    }
}

