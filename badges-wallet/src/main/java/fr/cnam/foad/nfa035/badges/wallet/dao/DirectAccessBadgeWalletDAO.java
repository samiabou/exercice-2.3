package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * interface définir le comportement  d'un DAO destinée à
 * la gestion de badge digital par acces direct
 *
 */

public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {

    /**
     * récupérer les metadonnées du wallet
     * @return
     * @throws IOException
     */
    List<DigitalBadgeMetadata> getWalletMetadata() throws Exception;

    /**
     * récupérer le badge du badge à partir de métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception;
}
