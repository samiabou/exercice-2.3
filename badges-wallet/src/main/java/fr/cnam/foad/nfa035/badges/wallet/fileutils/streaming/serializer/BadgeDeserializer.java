package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * interface définir le comportement d'un objet servant  à la désérialisation d'une image à partir d'un média.
 *
 * @param <M>
 */
public interface BadgeDeserializer<M extends ImageFrameMedia> {

    /**
     * méthode de désérialisation permett de transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    void deserialize(M media) throws IOException;

    /**
     * utile pour récupérer un Flux d'écriture sur la source
     *
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     * utile pour modifier le flux de sortie au format source
     *
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);


}
