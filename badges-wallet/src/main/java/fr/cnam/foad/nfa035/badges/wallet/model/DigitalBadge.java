package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Objects;

public class DigitalBadge {

    private DigitalBadgeMetadata metadata;
    private File badge;

    DigitalBadge(File badge,DigitalBadgeMetadata metadata) {
        this.badge = badge;
        this.metadata = metadata;
    }


    @Override
    public boolean equals (Object o){
        if(this == o)return true;
        if (o == null|| getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;

        return false;
    }

        /**
     * Getter des metadonnées
     * @return
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge
     * @return le badge
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Setter badge
     * @param badge
     */

    public void setBadge(File badge) {
        this.badge = badge;
    }

    @Override
    public int hashCode(){
        return Objects.hash(metadata,badge);
    }
}
