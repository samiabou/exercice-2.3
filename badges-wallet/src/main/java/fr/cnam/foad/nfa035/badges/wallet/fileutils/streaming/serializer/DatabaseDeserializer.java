package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;


public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param data
     * @param <K>
     * @return
     * @throws IOException
     */
    <K extends InputStream> K getDeserializingStream(String data) throws IOException;


}
