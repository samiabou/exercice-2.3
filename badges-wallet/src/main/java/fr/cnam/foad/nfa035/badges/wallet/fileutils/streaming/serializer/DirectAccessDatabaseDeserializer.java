package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;

/**
 * interface définir le comportement  d'un objet servant  à la désérialisation d'une image,
 * spécialiser pour un wallet en accès direct
 *
 */

public interface DirectAccessDatabaseDeserializer extends DatabaseDeserializer<WalletFrameMedia> {
    /**
     * méthode de dé désérialisation
     * @param media
     * @param meta
     * @throws IOException
     */

    void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException;

    /**
     * utile récupérer un flux d'écriture
     * @param <T>
     * @return
     */
    <T extends OutputStream> T getSourceOutputStream();

    /**
     *  utile pour modifier le flux de sortie
     * @param os
     * @param <T>
     */
    <T extends OutputStream> void setSourceOutputStream(T os);
}
