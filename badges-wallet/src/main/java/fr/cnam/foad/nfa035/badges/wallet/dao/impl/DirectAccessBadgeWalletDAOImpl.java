package fr.cnam.foad.nfa035.badges.wallet.dao.impl;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletDeserializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.List;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet
 * à badges digitaux multiples et par acces direct en prenant
 * en compte les métadonnées de chaque badges.
 */
public class DirectAccessBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {
    private final File walletDatabase;

    /**
     * Constructeur
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.walletDatabase = new File(dbPath);
    }

    /**
     * ajouter le badge
     * @param image
     * @throws Exception
     */
    @Override
    public void addBadge(File image) throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl();
            serializer.serialize(image,media);
        }
    }
    /**
     * récupérer le badge
     * @param imageStream
     * @throws Exception
     */
    @Override
    public void getBadge(OutputStream imageStream) throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseImpl(imageStream).deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     * @return
     * @throws Exception
     */

    @Override
    public List<DigitalBadgeMetadata> getWalletMetadata() throws Exception {
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return new MetadataDeserializerDatabaseImpl().deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     * @param imageStream
     * @param meta
     * @throws Exception
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws Exception {
        List<DigitalBadgeMetadata> metas = this.getWalletMetadata();
        try (WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rws"))) {
            new WalletDeserializerDirectAccessImpl().deserialize(media);
        }
    }
}
